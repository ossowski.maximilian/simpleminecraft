package pl.maxossowski.Minecraft;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class BlockDiamond extends Block {


    public BlockDiamond() {
        super();
        blockDurability = 75;
        numberOfDrawBlocks = 2;
    }

    @Override
    List<Item> gather(Tool tool) {
        Diamond diamond = new Diamond();
        ArrayList<Item> blocklist = new ArrayList<>();
        int toolDurability = 1;
        int minedBlocks;
        if (tool != null) {
            toolDurability = tool.toolDurability;
        }

        if (toolDurability < this.blockDurability) {
            System.out.println("You cant mine this block! Improper tool.");
            return null;
        }

        minedBlocks = drawNumberOfBlocks(numberOfDrawBlocks);
        System.out.println("Number of " + diamond.toString() + " gathered: " + minedBlocks);
        while (minedBlocks > 0) {
            blocklist.add(diamond);
            minedBlocks -= 1;
        }

        return blocklist;

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockDiamond.class.getSimpleName(), "")
                .toString();
    }
}
