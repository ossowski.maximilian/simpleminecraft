package pl.maxossowski.Minecraft;

import java.util.StringJoiner;

public class BlockIron extends Block {


    public BlockIron() {
        super();
        this.blockDurability = 50;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockIron.class.getSimpleName(), "")
                .toString();
    }
}
