package pl.maxossowski.Minecraft;

import java.util.Scanner;

class Game {

    private Player player = new Player();
    private GameMap gameMap = new GameMap(player);
    private boolean endGameFlag = true;
    private Scanner input = new Scanner(System.in);
    private Direction selectedDirection;

    void startGame() {
        System.out.println("Welcome to MiniMincraft. To win the game collect 3 units of diamonds.");
        System.out.println(" ");
        gameMap.populateMap();
        gameMap.showMap();
        while (endGameFlag) {

            if(player.checkWinCondition()){
                exitGame();
                continue;
            }

            System.out.println("Command list: move, gather, craft, chooseTool, showEq, showMap, exit");
            System.out.println(" ");
            System.out.print("Please enter command: ");
            String command = input.next();

            if (command.equals("exit")) {
                exitGame();
            }

            if (command.equals("craft")) {
                System.out.println("Choose Tool to craft: (woodenPickax, stonePickax, steelPickax)");
                String selectedTool = input.next();
                player.craft(selectedTool);
                player.printEquipmentList();
            }

            if (command.equals("move")) {
                System.out.println("Choose direction of move (up, down, left, right): ");
                String moveDirection = input.next();
                switch (moveDirection) {
                    case "up":
                        selectedDirection = Direction.UP;
                        break;
                    case "down":
                        selectedDirection = Direction.DOWN;
                        break;
                    case "left":
                        selectedDirection = Direction.LEFT;
                        break;
                    case "right":
                        selectedDirection = Direction.RIGHT;
                        break;
                    default:
                        System.out.println("Selected wrong direction");
                        break;
                }
                gameMap.movePlayer(selectedDirection);
                gameMap.showMap();
            }

            if (command.equals("showEq")) {
                player.printEquipmentList();
            }

            if (command.equals("gather")) {
                System.out.println("Choose which block to gather (up, down, left, right): ");
                String gatherDirection = input.next();
                switch (gatherDirection) {
                    case "up":
                        selectedDirection = Direction.UP;
                        break;
                    case "down":
                        selectedDirection = Direction.DOWN;
                        break;
                    case "left":
                        selectedDirection = Direction.LEFT;
                        break;
                    case "right":
                        selectedDirection = Direction.RIGHT;
                        break;
                    default:
                        System.out.println("Selected wrong direction");
                        break;
                }
                gatherBlock(selectedDirection);
                gameMap.showMap();
            }

            if (command.equals("showMap")) {
                gameMap.showMap();
            }

            if (command.equals("chooseTool")) {
                player.chooseTool();
            }

        }
        System.out.println("Thank you for playing!");
    }

    private void exitGame() {
        endGameFlag = false;
    }

    private void gatherBlock(Direction direction) {
        Block gatherBlock = gameMap.selectedBlockToGather(selectedDirection);
        if (gatherBlock == null) {
            System.out.println("There is no block to gather!");
        }
    }

}
