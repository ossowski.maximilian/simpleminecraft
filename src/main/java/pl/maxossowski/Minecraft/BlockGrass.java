package pl.maxossowski.Minecraft;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class BlockGrass extends Block{

    public BlockGrass() {
        super();
        this.blockDurability = 0;
    }

    @Override
    List<Item> gather(Tool tool) {
        ArrayList<Item> blocklist = new ArrayList<>();
        int toolDurability = 1;
        int minedBlockes;
        if (tool != null) {
            toolDurability = tool.toolDurability;
        }

        if (toolDurability < blockDurability) {
            System.out.println("You cant mine this block! Improper tool.");
            return null;
        }

        minedBlockes = drawNumberOfBlocks(3);
        System.out.println("Number of " + this.toString() + " gathered: " + minedBlockes);
        while (minedBlockes > 0) {
            blocklist.add(new BlockDirt());
            minedBlockes -= 1;
        }

        return blocklist;

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockGrass.class.getSimpleName(), "")
                .toString();
    }
}
