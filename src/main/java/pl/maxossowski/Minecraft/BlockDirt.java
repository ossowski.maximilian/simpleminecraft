package pl.maxossowski.Minecraft;

import java.util.StringJoiner;

public class BlockDirt extends Block{

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockDirt.class.getSimpleName(), "")
                .toString();
    }
}
