package pl.maxossowski.Minecraft;

import java.util.StringJoiner;

public class BlockStone extends Block {

    public BlockStone() {
        super();
        this.blockDurability = 25;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockStone.class.getSimpleName(), "")
                .toString();
    }
}
