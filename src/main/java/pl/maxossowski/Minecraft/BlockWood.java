package pl.maxossowski.Minecraft;

import java.util.StringJoiner;

public class BlockWood extends Block {

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockWood.class.getSimpleName(), "")
                .toString();
    }
}
