package pl.maxossowski.Minecraft;

public class Pickax extends Tool {

    private String name;

    Pickax(Block block) {
        if(block.getClass().equals(BlockWood.class)){
            toolDurability = 26;
            name = "WoodenPickax";
        }
        if(block.getClass().equals(BlockStone.class)){
            toolDurability = 51;
            name = "StonePickax";
        }
        if(block.getClass().equals(BlockIron.class)){
            toolDurability = 76;
            name = "SteelPickax";
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
