package pl.maxossowski.Minecraft;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;

public class Block implements Item, DrawableOnMap {

    Integer blockDurability;
    Integer numberOfDrawBlocks;

    public Block() {
        this.blockDurability = 0;
        this.numberOfDrawBlocks = 3;
    }

    List<Item> gather(Tool tool) {

        ArrayList<Item> blocklist = new ArrayList<>();
        int toolDurability = 1;
        int minedBlocks;
        if (tool != null) {
            toolDurability = tool.toolDurability;
        }

        if (toolDurability < this.blockDurability) {
            System.out.println("You cant mine this block! Improper tool.");
            return null;
        }

        minedBlocks = drawNumberOfBlocks(numberOfDrawBlocks);
        System.out.println("Number of " + this.toString() + " gathered: " + minedBlocks);
        while (minedBlocks > 0) {
            blocklist.add(this);
            minedBlocks -= 1;
        }

        return blocklist;

    }

    protected int drawNumberOfBlocks(int bound) {
            Random count = new Random();
            return count.nextInt(bound) + 1;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Block.class.getSimpleName(), "")
                .toString();
    }
}
