package pl.maxossowski.Minecraft;

import java.util.ArrayList;
import java.util.List;

class GameMap {

    private List<List<DrawableOnMap>> map;
    private Player player;

    GameMap(Player player) {
        this.player = player;
        this.map = new ArrayList<>();
    }

    void movePlayer(Direction direction) {
        List<Integer> newPlayerPosition = new ArrayList<>();
        List<Integer> oldPlayerPosition = new ArrayList<>();

        oldPlayerPosition.add(player.getPositionOnMap().get(0));
        oldPlayerPosition.add(player.getPositionOnMap().get(1));

        switch (direction) {
            case UP: {
                if (player.getPositionOnMap().get(0) > 0) {
                    String blockOnWay = map.get(player.getPositionOnMap().get(0) - 1).get(player.getPositionOnMap().get(1)).toString();
                    if (blockOnWay.equals(new EmptyBlock().toString())) {
                        newPlayerPosition.add(0, player.getPositionOnMap().get(0) - 1);
                    } else {
                        newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                    }
                } else {
                    newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                }
                newPlayerPosition.add(1, player.getPositionOnMap().get(1));
                break;
            }

            case DOWN: {
                if (player.getPositionOnMap().get(0) < map.get(0).size() - 1) {
                    String blockOnWay = map.get(player.getPositionOnMap().get(0) + 1).get(player.getPositionOnMap().get(1)).toString();
                    if (blockOnWay.equals(new EmptyBlock().toString())) {
                        newPlayerPosition.add(0, player.getPositionOnMap().get(0) + 1);
                    } else {
                        newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                    }
                } else {
                    newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                }
                newPlayerPosition.add(1, player.getPositionOnMap().get(1));
                break;
            }

            case LEFT: {
                newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                if (player.getPositionOnMap().get(1) > 0) {
                    String blockOnWay = map.get(player.getPositionOnMap().get(0)).get(player.getPositionOnMap().get(1) - 1).toString();
                    if (blockOnWay.equals(new EmptyBlock().toString())) {
                        newPlayerPosition.add(1, player.getPositionOnMap().get(1) - 1);
                    } else {
                        newPlayerPosition.add(1, player.getPositionOnMap().get(1));
                    }
                } else {
                    newPlayerPosition.add(1, player.getPositionOnMap().get(1));
                }
                break;
            }

            case RIGHT: {
                newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                if (player.getPositionOnMap().get(1) < map.size() - 1) {
                    String blockOnWay = map.get(player.getPositionOnMap().get(0)).get(player.getPositionOnMap().get(1) + 1).toString();
                    if (blockOnWay.equals(new EmptyBlock().toString())) {
                        newPlayerPosition.add(1, player.getPositionOnMap().get(1) + 1);
                    } else {
                        newPlayerPosition.add(1, player.getPositionOnMap().get(1));
                    }
                } else {
                    newPlayerPosition.add(1, player.getPositionOnMap().get(1));
                }
                break;
            }
            default:
                newPlayerPosition.add(0, player.getPositionOnMap().get(0));
                newPlayerPosition.add(1, player.getPositionOnMap().get(1));
        }

        updatePlayerMap(oldPlayerPosition, newPlayerPosition);
        player.setPositionOnMap(newPlayerPosition);
    }

    Block selectedBlockToGather(Direction direction) {
        DrawableOnMap selectedBlock;
        Block blockToGather = null;
        List<DrawableOnMap> rowToGather;
        Integer blockRowPosition;
        Integer blockColumnPosition;

        switch (direction) {
            case RIGHT: {
                blockRowPosition = player.getPositionOnMap().get(0);
                blockColumnPosition = player.getPositionOnMap().get(1) + 1;

                rowToGather = map.get(blockRowPosition);
                if (blockColumnPosition <= map.get(0).size() - 1) {
                    selectedBlock = rowToGather.get(blockColumnPosition);
                    if (!selectedBlock.toString().equals("EmptyBlock")) {
                        blockToGather = (Block) selectedBlock;
                    }

                    System.out.println(selectedBlock);
                    if (player.gatherBlock(blockToGather)) {
                        eraseBlockAfterGather(blockRowPosition, blockColumnPosition);
                    }

                }
                return blockToGather;

            }
            case LEFT: {
                blockRowPosition = player.getPositionOnMap().get(0);
                blockColumnPosition = player.getPositionOnMap().get(1) - 1;

                rowToGather = map.get(blockRowPosition);
                if (blockColumnPosition >= 0) {
                    selectedBlock = rowToGather.get(blockColumnPosition);
                    if (!selectedBlock.toString().equals("EmptyBlock")) {
                        blockToGather = (Block) selectedBlock;
                    }
                    System.out.println(selectedBlock);
                    if (player.gatherBlock(blockToGather)) {
                        eraseBlockAfterGather(blockRowPosition, blockColumnPosition);
                    }
                }
                return blockToGather;
            }
            case UP: {
                blockRowPosition = player.getPositionOnMap().get(0) - 1;
                blockColumnPosition = player.getPositionOnMap().get(1);

                if (blockRowPosition >= 0) {
                    rowToGather = map.get(blockRowPosition);
                    selectedBlock = rowToGather.get(blockColumnPosition);
                    if (!selectedBlock.toString().equals("EmptyBlock")) {
                        blockToGather = (Block) selectedBlock;
                    }
                    System.out.println(selectedBlock);
                    if (player.gatherBlock(blockToGather)) {
                        eraseBlockAfterGather(blockRowPosition, blockColumnPosition);
                    }
                }
                return blockToGather;
            }
            case DOWN: {
                blockRowPosition = player.getPositionOnMap().get(0) + 1;
                blockColumnPosition = player.getPositionOnMap().get(1);

                if (blockRowPosition <= map.get(1).size() - 1) {
                    rowToGather = map.get(blockRowPosition);
                    selectedBlock = rowToGather.get(blockColumnPosition);
                    if (!selectedBlock.toString().equals("EmptyBlock")) {
                        blockToGather = (Block) selectedBlock;
                    }
                    System.out.println(selectedBlock);
                    if (player.gatherBlock(blockToGather)) {
                        eraseBlockAfterGather(blockRowPosition, blockColumnPosition);
                    }
                }
                return blockToGather;
            }
            default:
                return null;


        }

    }

    void populateMap() {
        EmptyBlock emptyBlock = new EmptyBlock();

        ArrayList<DrawableOnMap> row = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            row.add(emptyBlock);
        }
        for (int i = 0; i < 10; i++) {
            ArrayList<DrawableOnMap> nextRow = (ArrayList<DrawableOnMap>) row.clone();
            map.add(nextRow);
        }
        row.clear();
        for (int i = 0; i < 10; i++) {
            Block testBlock = new Block();
            row.add(testBlock);
        }
        for (int i = 4; i < 10; i++) {
            ArrayList<DrawableOnMap> nextRow = (ArrayList<DrawableOnMap>) row.clone();
            map.set(i, nextRow);
        }

        // Set Player
        setBlockOnMap(player.getPositionOnMap().get(0), player.getPositionOnMap().get(1), player);

        // Tree on Map
        for (int i = 0; i < 4; i++) {
            setBlockOnMap(i, 7, new BlockWood());
        }

        // Set Grass on Map
        for (int i = 0; i < 10; i++) {
            setBlockOnMap(4, i, new BlockGrass());
        }

        // Set Dirt on Map
        for (int i = 0; i < 10; i++) {
            setBlockOnMap(5, i, new BlockDirt());
        }
        setBlockOnMap(6, 0, new BlockDirt());
        setBlockOnMap(6, 1, new BlockDirt());
        setBlockOnMap(6, 3, new BlockDirt());
        setBlockOnMap(6, 5, new BlockDirt());
        setBlockOnMap(6, 6, new BlockDirt());

        // Set Stone
        setBlockOnMap(6, 2, new BlockStone());
        setBlockOnMap(6, 4, new BlockStone());
        setBlockOnMap(6, 7, new BlockStone());
        setBlockOnMap(6, 8, new BlockStone());
        setBlockOnMap(6, 9, new BlockStone());
        for (int i = 0; i < 10; i++) {
            setBlockOnMap(7, i, new BlockStone());
            setBlockOnMap(8, i, new BlockStone());
            setBlockOnMap(9, i, new BlockStone());
        }

        // Set Iron
        setBlockOnMap(7, 7, new BlockIron());
        setBlockOnMap(8, 8, new BlockIron());
        setBlockOnMap(7, 9, new BlockIron());

        // Set Diamonds
        setBlockOnMap(9, 1, new BlockDiamond());
        setBlockOnMap(9, 3, new BlockDiamond());
    }


    void showMap() {
        boolean flagFirstRow = false;
        int flagRowCounter = 0;
        System.out.println("    MAP     ");
        System.out.println("----------");
        for (List<DrawableOnMap> drawColumnItem : map) {
            if (flagFirstRow) {
                System.out.println(" ");
            }
            for (DrawableOnMap drawRowItem : drawColumnItem) {
                flagFirstRow = true;
                System.out.print(drawSymbol(drawRowItem));
            }
            if (flagRowCounter == 1) {
                System.out.print("      LEGEND:     ");
            }
            if (flagRowCounter == 3) {
                System.out.print("      X - Player  ");
            }
            if (flagRowCounter == 4) {
                System.out.print("      W - Wood    ");
            }
            if (flagRowCounter == 5) {
                System.out.print("      G - Grass    ");
            }
            if (flagRowCounter == 6) {
                System.out.print("      D - Dirt    ");
            }
            if (flagRowCounter == 7) {
                System.out.print("      S - Stone    ");
            }
            if (flagRowCounter == 8) {
                System.out.print("      I - Iron    ");
            }
            if (flagRowCounter == 9) {
                System.out.print("      O - Diamonds    ");
            }


            flagRowCounter++;
        }
        System.out.println(" ");
        System.out.println("----------");
    }

    private void setBlockOnMap(int indexRow, int indexColumn, DrawableOnMap item) {
        List<DrawableOnMap> rowArray = map.get(indexRow);
        rowArray.set(indexColumn, item);
        map.set(indexRow, rowArray);
    }

    private void eraseBlockAfterGather(Integer rowBlockPosition, Integer columnBlockPosition) {
        setBlockOnMap(rowBlockPosition, columnBlockPosition, new EmptyBlock());
    }

    private void updatePlayerMap(List<Integer> oldPlayerPosition, List<Integer> newPlayerPosition) {

        List<DrawableOnMap> oldRowToChange = map.get(oldPlayerPosition.get(0));
        oldRowToChange.set(oldPlayerPosition.get(1), new EmptyBlock());

        List<DrawableOnMap> newRowToChange = map.get(newPlayerPosition.get(0));
        newRowToChange.set(newPlayerPosition.get(1), player);
    }


    private String drawSymbol(DrawableOnMap item) {
        switch (item.toString()) {
            case "Block":
                return "B";
            case "Player":
                return "X";
            case "BlockWood":
                return "W";
            case "BlockGrass":
                return "G";
            case "BlockDirt":
                return "D";
            case "BlockStone":
                return "S";
            case "BlockIron":
                return "I";
            case "BlockDiamond":
                return "O";
            default:
                return " ";
        }
    }

}
