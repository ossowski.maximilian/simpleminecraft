package pl.maxossowski.Minecraft;

import java.util.StringJoiner;

public class EmptyBlock implements DrawableOnMap{
    @Override
    public String toString() {
        return new StringJoiner(", ", EmptyBlock.class.getSimpleName(), "")
                .toString();
    }
}
