package pl.maxossowski.Minecraft;

import java.util.StringJoiner;

public class Diamond implements Item {

    @Override
    public String toString() {
        return new StringJoiner(", ", Diamond.class.getSimpleName(), "")
                .toString();
    }

}
