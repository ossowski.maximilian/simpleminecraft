package pl.maxossowski.Minecraft;



import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

public class Player implements DrawableOnMap {

    private List<Item> equipment;
    private Tool toolInHand;
    private List<Integer> positionOnMap;

    Player() {
        this.equipment = new ArrayList<>();
        this.toolInHand = null;
        positionOnMap = new ArrayList<>();
        positionOnMap.add(3);
        positionOnMap.add(3);
    }

    boolean gatherBlock(Block blockToGather) {
        if (blockToGather != null) {
            List<Item> gatheredBlocks = blockToGather.gather(toolInHand);
            if (gatheredBlocks != null) {
                addToEquipment(gatheredBlocks);
                return true;
            }
        }
        return false;
    }

    void printEquipmentList() {
        System.out.println("-------------------------");
        System.out.println(" Equipment List: ");
        if (equipment != null) {
            for (Item item : equipment) {
                System.out.println(" - " + item);
            }
        }
        System.out.println("-------------------------");
    }

    boolean checkWinCondition(){
        Diamond diamond = new Diamond();
        if(this.searchForItemInEq(diamond,3)){
            System.out.println("You have won the game!");
            return true;
        }
        return false;
    }

    void chooseTool() {
        List<Item> toolList = searchForToolInEq();
        if (toolList.size() == 0) {
            System.out.println("You have no tools in the Equipment!");
        } else {
            int i = 0;
            Scanner input = new Scanner(System.in);
            for (Item item : toolList) {
                System.out.println(i + ") " + item.toString());
                i++;
            }
            System.out.println("Choose tool number: ");
            int number = input.nextInt();
            toolInHand = (Tool) toolList.get(number);
        }
        System.out.println("Tool in hand is: " + this.toolInHand);
    }

    List<Integer> getPositionOnMap() {
        return positionOnMap;
    }

    void setPositionOnMap(List<Integer> positionOnMap) {
        this.positionOnMap = positionOnMap;
    }

    private void addToEquipment(List<Item> newStuffList) {
        if (newStuffList != null) {
            equipment.addAll(newStuffList);
        }
    }

    void craft(String selectedTool) {
        List<Item> newTool = new ArrayList<>();
        switch (selectedTool) {
            case "woodenPickax":
                if (searchForItemInEq(new BlockWood(), 3)) {
                    eraseItemFromEq(new BlockWood(), 3);
                    newTool.add(new Pickax(new BlockWood()));
                } else {
                    System.out.println("Not enough wood. You need 3 Woodblocks!");
                }
                break;
            case "stonePickax":
                if (searchForItemInEq(new BlockStone(), 3)) {
                    if (searchForItemInEq(new BlockWood(), 1)) {
                        eraseItemFromEq(new BlockStone(), 3);
                        eraseItemFromEq(new BlockWood(), 1);
                        newTool.add(new Pickax(new BlockStone()));
                    } else {
                        System.out.println("Not enough wood. You need 3 Stoneblock's and 1 Woodblock!");
                    }
                } else {
                    System.out.println("Not enough stone. You need 3 Stoneblock's and 1 Woodblock!");
                }
                break;
            case "steelPickax":
                if (searchForItemInEq(new BlockIron(), 3)) {
                    if (searchForItemInEq(new BlockWood(), 1)) {
                        eraseItemFromEq(new BlockIron(), 3);
                        eraseItemFromEq(new BlockWood(), 1);
                        newTool.add(new Pickax(new BlockIron()));
                    } else {
                        System.out.println("Not enough wood. You need 3 Ironblock's and 1 Woodblock!");
                    }
                } else {
                    System.out.println("Not enough iron. You need 3 Ironblock's and 1 Woodblock!");
                }
                break;
            default:
                System.out.println("Non proper tool selected");
        }


        addToEquipment(newTool);
    }

    private boolean searchForItemInEq(Item itemForSearch, int neededBlock) {
        int count = 0;
        for (Item item : equipment) {
            if (item.getClass().equals(itemForSearch.getClass())) {
                count++;
            }
        }
        return neededBlock <= count;
    }

    private void eraseItemFromEq(Item block, int count) {
        Item itemToErase = null;
        if (count > 0) {
            for (; count > 0; count--) {
                for (Item item : equipment) {
                    if (item.getClass().equals(block.getClass())) {
                        itemToErase = item;
                    }
                }
                equipment.remove(itemToErase);
            }
        } else {
            System.out.println("The count number needs to be grater then zero!");
        }

    }

    private List<Item> searchForToolInEq() {
        List<Item> toolList = new ArrayList<>();
        for (Item item : equipment) {
            if (item.getClass().isAssignableFrom(Pickax.class)) {
                toolList.add(item);
            }
        }
        return toolList;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", Player.class.getSimpleName(), "")
                .toString();
    }
}
