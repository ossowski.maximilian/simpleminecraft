Specification:

Simple Minecraft
    - Interface Item (marker interface)
    - Basic class Block implements Item
    - Tool (abstract) -> Pickax, Axe (wood, stone, steel)
    - Class Block: method -> public List<Item> gather(Tool tool)
    - Class BlockDirt
    - Class BlockGrass (returns BlockDirt)
    - Class BlockStone (needs PickAxe of any kind)
    - Class BlockWood
    - Class BlockIron (needs PickAxe stone)
    - Class BlockDiamond (needs PickAxe steel, returns 2-5 * Diamond)
    - Player -> Equipment (List<Item>)

Architecture:
- basic console Application

Technologies/Tools:
- OOP
